package com.java.Imminent.ServerInstance;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import com.java.Imminent.LoggingModule;

public class CentralizedServer implements Runnable
{
    private ServerSocket ss;
    PrintWriter pwOut; // we don't need br, since, we're using the sever as ip exchange
    private int port;

    public CentralizedServer( int port ) throws IOException
    {
        this.port=port;
        this.ss = new ServerSocket(port);
    }

    public void listenIncomingCon ( )
    {
        try{
            LoggingModule.log ( "listening for incoming cn", CentralizedServer.class );    
            Socket socket = this.ss.accept();
            LoggingModule.log( "received connection", CentralizedServer.class);
            Member mm = new Member(socket.getPort(), socket);
            this.pwOut = new PrintWriter(socket.getOutputStream(), false); // settigns the autoflush option on
            // mms.add( mm );
            this.broadcastIP(mm.getPort()); // here upon receiving the ip, they add it as peers
        }
        catch (IOException e )
        {
            LoggingModule.log(e.toString(), CentralizedServer.class );
        }
    }

    public void broadcastIP(int port)
    {
        // for ( Member m : mms )
        // {
        //     if ( m.getPort() != port )
        //     {
        //         // sending to all the users the ip
        //         LoggingModule.log("broadcasting someIP", CentralizedServer.class);
        //         m.getWOut().write(port);
        //         m.getWOut().flush();
        //     }
        // }
    }

    @Override
    public void run() {

    }

}