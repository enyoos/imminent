package com.java.Imminent.ServerInstance;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import com.java.Imminent.LoggingModule;

public class Member {
    public Member( int p, Socket s )
    {
        this.p=p;
        this.sckt=s;
        try{
            this.out = new PrintWriter(s.getOutputStream(), false);
        }
        catch ( IOException e )
        {
            LoggingModule.log ( e.toString(), Member.class );
        }
    } 

    public Member( Socket s )
    {
        this ( s.getPort(), s );
    }

    public PrintWriter getWOut()
    {
        return this.out;
    }

    public int getPort ( )
    {
        return this.p;
    }
    
    public Socket getSckt ( )
    {
        return this.sckt;
    }

    private PrintWriter out;
    private int p; // since we're running this p2p locally
    private Socket sckt;
    // private int ip;
}
