package com.java.Imminent;

public class App 
{
    private static final int PORT = 5555;
    private static final int PEERPORT = 6666;
    public static void main( String[] args )
    {
        Node node = new Node( PORT );
        node.addPeer(new PeerInfo(PEERPORT));
        System.out.println(node.getHost());
        node.mainLoop();
    }
}
