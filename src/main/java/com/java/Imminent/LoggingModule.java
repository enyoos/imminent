package com.java.Imminent;

public class LoggingModule {
    public static void log( String msg , Class<?> clr)
    {
        System.out.println(String.format("[classname %s] %s", clr.getSimpleName(), msg));
    }
}
