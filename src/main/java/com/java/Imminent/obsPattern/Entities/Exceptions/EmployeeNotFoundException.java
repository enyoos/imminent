package com.java.Imminent.obsPattern.Entities.Exceptions;

public class EmployeeNotFoundException extends RuntimeException { // why the runtime ? 

  public EmployeeNotFoundException(Long id) {
    super("Could not find employee " + id);
  }
}