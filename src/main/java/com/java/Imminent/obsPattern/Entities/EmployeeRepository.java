package com.java.Imminent.obsPattern.Entities;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
@Service
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
    // the findbyId method retruns an optinal
    // this is not needed since we're only querying with the id params.
    // Employee findByName ( String name ); // name shoudl be unique 
    // List<Employee> findByRole ( String role );
    // List<Tutorial> findByPublished(boolean published);
    // List<Tutorial> findByTitleContaining(String title);
}
