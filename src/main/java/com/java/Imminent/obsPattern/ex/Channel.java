package com.java.Imminent.obsPattern.ex;

public interface Channel {
    public void update(Object o);
}