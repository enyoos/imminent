package com.java.Imminent.obsPattern.ex;

public class NewsChannel implements Channel {
    private String news;

    @Override
    public void update(Object news) {
        this.setNews((String) news);
    } 
    // standard getter and setter
    public String getNews ( )
    {
        return this.news;
    }

    public void setNews(String c )
    {
        this.news = c; 
    }
}

