package com.java.Imminent.obsPattern.ex;

public class Main {
    
    public static void main( String... rgs )
    {
        NewsAgency na = new NewsAgency();
        NewsChannel nc = new NewsChannel();
        na.addObserver(nc);
        String content = "helloworld";
        na.setNews(content);
        System.out.println(nc.getNews());
    }
}

