package com.java.Imminent.obsPattern;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

import javax.annotation.PreDestroy;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javafx.application.Application;

@SpringBootApplication
@RestController
public class MyApp {
    private static final String NAME = "my app";
    private static final String FILENAME = "LogFile.err";
    private static ConfigurableApplicationContext context;

    // for the log err file
    // private static final PrintStream ps = new PrintStream( new FileReader(FILENAME));

    public static void main(String[] args) {
        context = SpringApplication.run(MyApp.class, args);
        openHomePage(); 
    }

    private static void openHomePage()  {
        try{
            Runtime rt = Runtime.getRuntime();
            rt.exec("rundll32 url.dll,FileProtocolHandler " + "http://localhost:8080");
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
    }

    // TODO : Add a simple restart cmd line
    // // programmatically restart the app
    // public static void restart() {
    //     System.out.println("restarting the app");
    //     ApplicationArguments args = context.getBean(ApplicationArguments.class);
    //     System.out.println("args: " + args);

    //     Thread thread = new Thread(() -> {
    //         context.close();
    //         context = SpringApplication.run(MyApp.class, args.getSourceArgs());
    //     });

    //     thread.setDaemon(false);
    //     thread.start();
    // }

    // @PreDestroy
    // public void onDestoy()
    // {
    //     System.out.println("destroying the app");
    // }

    // public static void destroy()
    // {
    //     System.out.println("closing the app");
    //     context.close();
    // }

    @GetMapping(path="/")
    public String mainPage(){
        return String.format("Hello %s! this link is serving a RESTFUL API", NAME);
    }    

    // @Component
    // private static class MyRunner implements CommandLineRunner {

    //     @Override
    //     public void run(String... args) throws Exception {
    //         boolean done = false;
    //         Scanner scanner = new Scanner(System.in);
    //         while ( !done ){
    //             System.out.println("Enter word!");
    //             String line = scanner.nextLine();
    //             if ( line.equalsIgnoreCase( "s") )
    //             {
    //                 System.out.println(String.format("[%s] listening...", NAME ));
    //             }
    //             else if ( line.equalsIgnoreCase("r"))
    //             {
    //                 restart();
    //             }
    //             else if ( line.equals("d"))
    //             {
    //                 destroy();
    //                 done = true;
    //             }
    //             else
    //             {
    //                 System.out.println("this command is not valid ( s, r, d )");
    //             }
    //         }
    //         scanner.close();
    //     }
    // }

    // @GetMapping(path="/error")
    // public String showErr() {
    //   return "something when wrong";
    // }
}
